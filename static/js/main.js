
var anchor = document.getElementById('anchor');
var searchField = document.getElementById('search');
var senderField = document.getElementById('sender');
var messageField = document.getElementById('message');
var searchBtn = document.getElementById('searchBtn');
var sendBtn = document.getElementById('sendBtn');
var logoutBtn = document.getElementById('logoutBtn')
var allBtn = document.getElementById('allBtn');
var output = document.getElementById('output');
var header = document.getElementById('header');
var allPostBtn = document.getElementById('allPostBtn');
var showHistoryBtn = document.getElementById('showHistoryBtn');


var search = async (query) => {
    if (query.length > 300) {
        query = '*'; }
    const csrfToken = document.querySelector("meta[name='csrf_token']").getAttribute("content");
    const q = `/show_conv?q=${encodeURIComponent(query)}`;
    res = await fetch(q, { headers: { "X-CSRF-Token": csrfToken},});
    const head = document.createElement('h3');
    output.appendChild(head);
    const body = document.createElement('p');
    dict = JSON.parse(await res.text());
    if (dict){
        to_board = "";
        for(let k in dict) {
            to_board += dict[k][0] + ' ('+ dict[k][1] +'): '+dict[k][2] + '\n';
        }
        head.textContent = 'Your conversation with '+ query+' : ';
        body.textContent = to_board;
        output.appendChild(body);
        body.scrollIntoView({block: "end", inline: "nearest", behavior: "smooth"});
        anchor.scrollIntoView();
    }
};
var show_history = async(query) => {
    const csrfToken = document.querySelector("meta[name='csrf_token']").getAttribute("content");
    if (query.length > 300) {
        query = '*'; }
    const q = `/show_user_history?q=${encodeURIComponent(query)}`;
    res = await fetch(q, { headers: { "X-CSRF-Token": csrfToken},});
    const head = document.createElement('h3');
    output.appendChild(head);
    const body = document.createElement('p');
    dict = JSON.parse(await res.text());
    if(dict) {
        to_board = "";
        for(let k in dict) {
            to_board += dict[k][0] + ' to ' +dict[k][3] +' ('+ dict[k][1] +'): '+dict[k][2] + '\n';
        }
    }
    head.textContent = 'Showing history of user '+ dict[0][0];
    body.textContent = to_board;
    output.appendChild(body);
    body.scrollIntoView({block: "end", inline: "nearest", behavior: "smooth"});
    anchor.scrollIntoView();
    
};
var show_all = async () => {
    const csrfToken = document.querySelector("meta[name='csrf_token']").getAttribute("content");
    q = '/show_all';
    //res = await fetch(q, { headers: { "X-CSRF-Token": csrfToken},});
    res = await fetch(q);
    const head = document.createElement('h3');
    output.appendChild(head);
    const body = document.createElement('p');
    dict = JSON.parse(await res.text());
    if(dict){
        to_board = "";
        for(let k in dict) {
            to_board += dict[k][0] + ' ('+ dict[k][1] +'): '+dict[k][2] + '\n';
        }

    }
   
    body.textContent = to_board;
    output.appendChild(body);
    body.scrollIntoView({block: "end", inline: "nearest", behavior: "smooth"});
    anchor.scrollIntoView();
}

var show_all_posts = async () => {
    const csrfToken = document.querySelector("meta[name='csrf_token']").getAttribute("content");
    q = '/show_all_posts';
    res = await fetch(q, { headers: { "X-CSRF-Token": csrfToken},});
    const head = document.createElement('h3');
    output.appendChild(head);
    const body = document.createElement('p');
    dict = JSON.parse(await res.text());
    if (dict){
        to_board = "";
        for(let k in dict) {
            to_board += dict[k][0] + ' to ' +dict[k][3] +' ('+ dict[k][1] +'): '+dict[k][2] + '\n';
         }
    }
    
    body.textContent = to_board;
    output.appendChild(body);
    body.scrollIntoView({block: "end", inline: "nearest", behavior: "smooth"});
    anchor.scrollIntoView();    
};
var send = async (sender, message) => {
    if (sender.length > 100 || message.length > 1000) {
        sender = "", message = "" }
    const csrfToken = document.querySelector("meta[name='csrf_token']").getAttribute("content");
    const q = `/send?sender=${encodeURIComponent(sender)}&message=${encodeURIComponent(message)}`;
    res = await fetch(q, { method: 'post', headers: { "X-CSRF-Token": csrfToken}, });
    const head = document.createElement('h3');
    output.appendChild(head);
    const body = document.createElement('p');
    dict = JSON.parse(await res.text());
    head.textContent = dict.author + ':';
    body.textContent = dict.message;
    output.appendChild(body);
    body.scrollIntoView({block: "end", inline: "nearest", behavior: "smooth"});
    anchor.scrollIntoView();

};
if (searchField) {
searchField.addEventListener('keydown', ev => {
    if (ev.key === 'Enter') {
        search(searchField.value);
    }
}); };
if (searchBtn) {
searchBtn.addEventListener('click', () => search(searchField.value)); };
allBtn.addEventListener('click', () => show_all());
sendBtn.addEventListener('click', () => send(senderField.value, messageField.value));
if (allPostBtn) {
    allPostBtn.addEventListener('click', () => show_all_posts());
}
if (showHistoryBtn) {
    showHistoryBtn.addEventListener('click', () => show_history(searchField.value));
}