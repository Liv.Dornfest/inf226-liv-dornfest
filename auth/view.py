from flask import redirect, url_for, flash, make_response, render_template
from flask_login import logout_user, login_user, login_required, current_user
from flask_login import login_required, login_user
from . import auth
from ..models import User
from .. import db
from ..forms import LoginForm, RegistrationForm
from werkzeug.security import generate_password_hash, check_password_hash



# Visit site as a guest
# Guests can write public announcements
# And see the public feed
@auth.get('/guest')
def guest():
    response = redirect(url_for('api.index_html'))
    return response

# Register as a user
# A user has access to new features:
# Private messages, separate inbox, new search function
@auth.route('/register', methods=['GET','POST'])
def register():
    """ Create a new user """
    form = RegistrationForm()

    if form.validate_on_submit():
        existing_user = db.session.query(User).filter(User.username == form.username.data).first()
        
        if existing_user or form.username.data == 'Guest':
            response = redirect(url_for('auth.register'))
            flash('Invalid username and/or password, please try again')
            return response
    
        new_user = User(
            username = form.username.data,
            password = generate_password_hash(form.password.data,method='sha256'),
            role = 'USER'
        )

        # added to make testing easier, automatically creates a moderator when 'bob' is created
        if form.username.data == 'bob':
            new_moderator = User(
                username = 'moderator',
                password = generate_password_hash('123456789',method='sha256'),
                role = 'MODERATOR'
        )
            db.session.add(new_moderator)

        db.session.add(new_user)
        db.session.commit()
        flash('Registered successfully.')
    response = make_response(render_template('./register.html', register_form = form))
    response.headers['Content-Type'] = 'text/html'
    return response

# Log in, start a new session with login_user
# Can retrieve old messages and write new!
@auth.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated: # if you are logged in, you cannot log in again
        response = redirect(url_for('api.index_html'))
        return response

    form = LoginForm()
    if form.validate_on_submit():
        user = db.session.query(User).filter(User.username == form.username.data).first()
        password = form.password.data
        if user and check_password_hash(user.password,password):
           
            login_user(user)       
            response = redirect(url_for('api.index_html'))
            return response

    response = make_response(render_template('./login.html', form = form))
    return response
  
# Log out from the session with logout_user
@auth.route('/logout')
@login_required
def logout():
    logout_user()
    response = redirect(url_for('auth.login'))
    return response


