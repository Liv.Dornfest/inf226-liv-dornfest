from flask import render_template, request, make_response, jsonify
import flask
from flask_login import login_required, current_user
from .. import db, admin_required
from ..models import Post,Announcement
from ..forms import MessageForm
from . import api

@api.route('/')
@api.route('/index.html')
def index_html():
    response = flask.make_response(render_template('./index.html',mimetype='text/html'))
    return response

# show all public announcements from the feed
@api.get('/show_all')
def show_all():
    result = db.session.query(Announcement).all()
    output = [[r.author, r.timestamp, r.body] for r in result]
    response = make_response(jsonify(output))
    return response

# show all private conversations
# only logged in moderators get this option
@api.get('/show_all_posts')
@login_required
@admin_required
def show_all_posts():
    result = db.session.query(Post).all()
    output = [[r.author, r.timestamp, r.body, r.recipient] for r in result]
    response = make_response(jsonify(output))
    return response

# show all of a user's sent messages (private)
# only logged in moderators get this option
@api.route('/show_user_history', methods=['GET', 'POST'])
@login_required 
@admin_required 
def show_user_history():
    user = request.args.get('q')
    all_messages = db.session.query(Post).all()
    history= []
    for msg in all_messages:
        if (msg.author == user):
            history.append(msg)
    output = [[r.author, r.timestamp, r.body, r.recipient] for r in history]
    response = make_response(jsonify(output))
    return response

# show the current user their conversation/private messages
# with another user
# must be logged in
@api.route('/show_conv', methods=['GET', 'POST'])
@login_required
def show_conv():
    user = request.args.get('q') or request.form.get('q')
    all_messages = db.session.query(Post).all()
    conversation = []
    for msg in all_messages:
        if (current_user.username == msg.author and user == msg.recipient) or (current_user.username == msg.recipient and user == msg.author):
            conversation.append(msg)
    output = [[r.author, r.timestamp, r.body] for r in conversation]
    response = make_response(jsonify(output))
    return response

# publish an announcement for all to see
# available for all, including guests/anonymous users
@api.route('/send', methods=['GET', 'POST'])
def send():
    message = request.args.get('message')
    if current_user == "Guest":
        this_author = "Guest"
    else:
        this_author = current_user.username
    announcement = Announcement(
        author = this_author,
        body = message)
    db.session.add(announcement)
    db.session.commit()
    response = make_response(jsonify(author = announcement.author, message = announcement.body))
    return response

# create a new private message
# must be logged in
@api.route('/new_post', methods=['GET', 'POST'])
@login_required
def new_post():
    form = MessageForm()
    if form.copy.data:
        this_copy = form.copy.data
    else:
        this_copy = None

    if form.validate_on_submit():
        new_post = Post(
            subject = form.subject.data,
            copy = this_copy,
            body = form.message.data,
            recipient = form.recipient.data,
            author = current_user.username
        )
        db.session.add(new_post)
        db.session.commit()
        response = make_response(render_template('index.html'))
        return response

    response = make_response(render_template('./post.html', message_form = form))
    return response

# get your private inbox
# must be logged in
@login_required
@api.route('/get_inbox', methods=['GET', 'POST'])
def get_inbox():
    all_messages = db.session.query(Post).filter(Post.recipient == current_user.username)
    response = make_response(render_template('./inbox.html', posts=all_messages))
    response.headers['Content-Type'] = 'text/html'
    return response

# get your private carbon copies
# must be logged in
@login_required
@api.route('/get_copy', methods=['GET', 'POST'])
def get_copy():
    all_messages = db.session.query(Post).filter(Post.copy == current_user.username)
    response = make_response(render_template('./copy.html', posts=all_messages))
    response.headers['Content-Type'] = 'text/html'
    return response

# get your sent private messages
# must be logged in
@login_required
@api.route('/get_outbox', methods=['GET', 'POST'])
def get_outbox():
    all_messages = db.session.query(Post).filter(Post.author == current_user.username)
    response = make_response(render_template('./outbox.html', posts=all_messages))
    response.headers['Content-Type'] = 'text/html'
    return response

