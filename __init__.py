from flask import Flask
from flask_login import LoginManager, current_user
from flask_sqlalchemy import SQLAlchemy
import os
from flask_bcrypt import Bcrypt
from flask_wtf import CSRFProtect
from flask_talisman import Talisman
from functools import wraps


basedir = os.path.abspath(os.path.dirname(__file__))
login_manager = LoginManager()
login_manager.login_view = "auth.login"

# configure the Content Security Policy
csp = {
    'default-src': '\'self\'',
    'frame-ancestors': '\'none\'',
    'form-action': '\'self\''
}
db = SQLAlchemy()
crsf = CSRFProtect() # protect against cross-site forgery


# decorator for role permission
# admin_required is only accessable for users
# who has the MODERATOR role
def admin_required(f):
        @wraps(f)
        def wrap(*args, **kwargs):
            if current_user.role == "MODERATOR":
                return f(*args, **kwargs)
            else:
                return None
        return wrap

        
def create_app():
    
    app = Flask(__name__)
    app.config['SECRET_KEY'] = os.urandom(32) # generate a 32 bytes secret key
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'users.db') # set database location
    
    # set security options
    # see more info at https://github.com/GoogleCloudPlatform/flask-talisman
    # if javascript gives you trouble on windows machine, disable X-Content-Type-Options = 'nosniff'
    talisman = Talisman(
        app,
        content_security_policy=csp,
        force_https = False # we are running this on HTTP, but normally this would be enabled
        )
    
    crsf.init_app(app)
    db.init_app(app)
    bcrypt = Bcrypt(app)
    login_manager.init_app(app)

    from .models import User,Post,Announcement 
    
    with app.app_context():
        db.create_all() # create sql tables for data models, if needed
   
    # blueprint for auth routes in our app
    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    # blueprint for api parts of app
    from .api import api as api_blueprint
    app.register_blueprint(api_blueprint)

    # careful! debug mode activated is security flaw
    app.debug = False

    # set Cache-Control to 'no-store', to enforce new log-in if user has logged out
    # "restoring tabs" or going back in the browser won't return you to a logged in session
    @app.after_request
    def add_header(response):
        response.cache_control.no_store = True
        if 'Cache-Control' not in response.headers:
            response.headers['Cache-Control'] = 'no-store'
        return response
        

    return app

