from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, validators

class LoginForm(FlaskForm):
    username = StringField('Username', [validators.InputRequired(), validators.Length(3,20, message='Please provide a valid name'), 
    validators.Regexp(
                "^[A-Za-z][A-Za-z0-9_.]*$",
                0,
                "Usernames must have only letters, " "numbers, dots or underscores",
            ),])
    password = PasswordField('Password', [validators.InputRequired(), validators.Length(8,72)])
    submit = SubmitField('Submit')

class RegistrationForm(FlaskForm):
    username = StringField('Username', [validators.InputRequired()])
    password = PasswordField('Password',[validators.InputRequired(), validators.Length(8,72)])
    submit = SubmitField('Submit')

class MessageForm(FlaskForm):
    recipient = StringField('Recipient', [validators.InputRequired()])
    copy = StringField('Cc')
    subject = StringField('Subject', [validators.InputRequired()])
    message = StringField('Message', [validators.InputRequired()])
    submit = SubmitField('Submit')

class AnnouncementForm(FlaskForm):
    message = StringField('Message', [validators.InputRequired()])
    submit = SubmitField('Submit')