from flask_login import UserMixin,AnonymousUserMixin
from datetime import datetime
import flask
from . import login_manager, db

# Potential for these fields to be more restricted
# There are for now restrictions in the forms, which controls what kind of Users, Posts etc can be created

# A user in the database
class User(UserMixin,db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer,primary_key=True)
    password = db.Column(db.String(100))
    username = db.Column(db.String(1000), unique=True)
    role = db.Column(db.String(100))

# Overwrite of the default AnonymousUserMixin class, to give it a role and a name
class Anonymous(AnonymousUserMixin):
    def __init__(self):
        self.username = 'Guest'
        self.role = 'GUEST'

# A private message
class Post(db.Model):
    __tablename__='posts'
    id = db.Column(db.Integer, primary_key = True)
    subject = db.Column(db.String(100))
    copy = db.Column(db.String(100))
    body = db.Column(db.String(1000))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    author = db.Column(db.String(100))
    recipient = db.Column(db.String(100))

# A public announcement
class Announcement(db.Model):
    __tablename__='announcements'
    id = db.Column(db.Integer, primary_key = True)
    body = db.Column(db.String(1000))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    author = db.Column(db.String(100))

# Overwrite default
login_manager.anonymous_user= Anonymous

# How the login manager gets a user
@login_manager.user_loader
def user_loader(user_id):
    user = User.query.get(user_id)
    if user:
        return user
    return None

# If the current user is unauthorized, return them to login
# (Although now they are instead called guests, and the inaccessible options are removed from their view)
@login_manager.unauthorized_handler
def unauthorized():
    response = flask.make_response(flask.redirect(flask.url_for('auth.login')))
    return response


